$(document).ready(function() {
  var view = new View();
  view.on("loaded", function(data) {
    console.log(data);
    var ul = $("#sceneList > ul");
    ul.empty(); // On vide le UL pour supprimer les liens appartenants a une autre visite
    $.each(data.scenes, function(index, item) { // Pour chaque scene définie dans le fichier json
      // On créé les différents éléments du menu
      var li = $(document.createElement("li"));
      var a = $(document.createElement("a"));
      var span = $(document.createElement("span"));
      var p = $(document.createElement("p"));
      p.text(item.name);
      p.addClass("p");
      span.append(p);
      a.append(span);
      span.addClass("glyphicons");
      span.addClass("glyphicons-"+item.icon);
      li.addClass("text");
      li.append(a);
      ul.append(li);
      a.attr("href", "#");
      a.addClass("scene");
      a.click(function() { // Quand on clique sur le lien, on change de scene
        view.switchScene(view.findSceneById(item.id));
        console.log(item);
      });
    });
  });

    view.load('src/data/unipy.json');
//     $("#audio-player")[0].volume=0;
//     $("#audio-player")[0].play();
//
//       $(document).on('click', '#viewSong', function(){
//         if($("#audio-player")[0].paused) {
//           $("#audio-player")[0].play();
//         }
//         else {
//               $("#audio-player")[0].pause();
//         }
// });
});

$(document).on('click', '#menuSceneList', function(){
  // console.log($('#sceneList').css('display'));
  if ('block' === $('#sceneList').css('display')) {
    $('#sceneList').css('display', 'none');
  }
  else {
    $('#sceneList').css('display', 'block');
  }
});

$(document).on('click', '#viewInfo', function(){
  if ('block' === $('#information').css('display')) {
    $('#information').css('display', 'none');
  }
  else {
    $('#information').css('display', 'block');
  }
});


$(document).on('click', '#toggleDeviceOrientation', function(){
if ('block' === $('#enable').css('display')) {
  $('#enable').css('display', 'none');
  $('#disable').css('display', 'block');
}
else {
  $('#enable').css('display', 'block');
  $('#disable').css('display', 'none');
}
});
