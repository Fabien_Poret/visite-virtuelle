$(document).ready(function() {
  var view = new View();
  view.on("loaded", function(data) {
    console.log(data);
    var ul = $("#sceneList > ul");
    ul.empty(); // On vide le UL pour supprimer les liens appartenants a une autre visite
    $.each(data.scenes, function(index, item) { // Pour chaque scene définie dans le fichier json
      // On créé les différents éléments du menu
      var li = $(document.createElement("li"));
      var a = $(document.createElement("a"));
      var span = $(document.createElement("span"));
      var p = $(document.createElement("p"));
      p.text(item.name);
      p.addClass("p");
      span.append(p);
      a.append(span);
      span.addClass("glyphicons");
      span.addClass("glyphicons-"+item.icon);
      li.addClass("text");
      li.append(a);
      ul.append(li);
      a.attr("href", "#");
      a.addClass("scene");
      a.click(function() { // Quand on clique sur le lien, on change de scene
        view.switchScene(view.findSceneById(item.id));
        console.log(item);
      });
    });
  });

  view.on('fullscreen', function(data) {
    var enabled = data.enabled;
    console.log("Enabled : " + enabled);
    if(true === enabled)
    {
      $("body").addClass('fullscreenEvent');
    }
    else {
      $("body").removeClass('fullscreenEvent');
    }
  });

  $(document).on('click', 'a', function(){
    var a = $(this);
    if(a.attr("data-360-id") !== undefined) {

      var id = a.attr("data-360-id");
      //Si le a est click alors charger la vr correspondant à l'id
      view.load('src/data/'+id+'.json');
      if($('#map').css('display') === 'block'){
        $('#map').css('display', 'none');
        $('#menu').css('display', 'block');
        $('#visite').text("Voir la map");
        $('#menuSceneList').css('display', 'block');
        $("#audio-player")[0].play();
        $('#search-bar').css('width', '100%');
        $("#audio-player")[0].volume=0.10;
      }
      else {
        $('#imgQrCode').css('display', 'none');
        $('#map').css('display', 'block');
        $('#visite').text("Visite en 360°");
        $('#menu').css('display', 'none');
        $('#menuSceneList').css('display', 'none');
        $('#search-bar').css('width', '96%');
        $("#audio-player")[0].pause();
        google.maps.event.trigger(window.map, 'resize');
     }

    }
  });

  $(document).on('click', '#pause', function(){
    $('#pause').css('display', 'none');
    $('#play').css('display', 'block');
  });

  $(document).on('click', '#play', function(){
    $('#pause').css('display', 'block');
    $('#play').css('display', 'none');
  });

  $(document).on('click', '#menu-right-nav', function(){
    // console.log($('#sceneList').css('display'));
    if ('block' === $('#sceneList').css('display')) {
      $('#sceneList').css('display', 'none');
    }
    else {
      $('#sceneList').css('display', 'block');
    }
  });


  $(document).on('click', '#qrCode', function(){
    console.log( $('#imgQrCode').css('display'));
    if (  $('#imgQrCode').css('display') === 'block') {
      $('#imgQrCode').css('display', 'none');
    }
    else {
      $('#imgQrCode').css('display', 'block');
    }
  });

  $(document).on('click', '#viewSong', function(){
    if($("#audio-player")[0].paused) {
      $("#audio-player")[0].play();
    }
    else {
      $("#audio-player")[0].pause();
    }
  });


  $(document).on('click', '#menuSceneList', function(){
    console.log($('#sceneList').css('display'));
    if ('block' === $('#sceneList').css('display')) {
      $('#sceneList').css('display', 'none');
    }
    else {
      $('#sceneList').css('display', 'block');
    }
  });


    $(function(icon){
    $(icon).mouseover(function(){
        icon.attr('src', 'assets/img/link-clic.png');
    });
    $(icon).mouseout(function(){
        icon.attr('src', 'assets/img/link.png');
    });
  });

});
