/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Custom control method to alter the view according to the device orientation.
function DeviceOrientationControlMethod() {
  this._dynamics = {
    yaw: new Marzipano.Dynamics(),
    pitch: new Marzipano.Dynamics()
  };

  this._deviceOrientationHandler = this._handleData.bind(this);

  if (window.DeviceOrientationEvent) {
    window.addEventListener('deviceorientation', this._deviceOrientationHandler);
  }

  this._previous = {};
  this._current = {};
  this._tmp = {};

  this._getPitchCallbacks = [];
}

Marzipano.dependencies.eventEmitter(DeviceOrientationControlMethod);


DeviceOrientationControlMethod.prototype.destroy = function() {
  this._dynamics = null;
  if (window.DeviceOrientationEvent) {
    window.removeEventListener('deviceorientation', this._deviceOrientationHandler);
  }
  this._deviceOrientationHandler = null;
  this._previous = null;
  this._current = null;
  this._tmp = null;
  this._getPitchCallbacks = null;
};


DeviceOrientationControlMethod.prototype.getPitch = function(cb) {
  this._getPitchCallbacks.push(cb);
};


DeviceOrientationControlMethod.prototype._handleData = function(data) {
  var previous = this._previous,
      current = this._current,
      tmp = this._tmp;

  tmp.yaw = Marzipano.util.degToRad(data.alpha);
  tmp.pitch = Marzipano.util.degToRad(data.beta);
  tmp.roll = Marzipano.util.degToRad(data.gamma);

  rotateEuler(tmp, current);

  // Report current pitch value.
  this._getPitchCallbacks.forEach(function(callback) {
    callback(null, current.pitch);
  });
  this._getPitchCallbacks.length = 0;

  // Emit control offsets.
  if (previous.yaw != null && previous.pitch != null && previous.roll != null) {
    this._dynamics.yaw.offset = -(current.yaw - previous.yaw);
    this._dynamics.pitch.offset = (current.pitch - previous.pitch);

    this.emit('parameterDynamics', 'yaw', this._dynamics.yaw);
    this.emit('parameterDynamics', 'pitch', this._dynamics.pitch);
  }

  previous.yaw = current.yaw;
  previous.pitch = current.pitch;
  previous.roll = current.roll;
};

// Taken from krpano's gyro plugin by Aldo Hoeben:
// https://github.com/fieldOfView/krpano_fovplugins/tree/master/gyro/
// For the math, see references:
// http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToMatrix/index.htm
// http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToEuler/index.htm
function rotateEuler(euler, result) {
  var heading, bank, attitude,
    ch = Math.cos(euler.yaw),
    sh = Math.sin(euler.yaw),
    ca = Math.cos(euler.pitch),
    sa = Math.sin(euler.pitch),
    cb = Math.cos(euler.roll),
    sb = Math.sin(euler.roll),

    matrix = [
      sh*sb - ch*sa*cb,   -ch*ca,    ch*sa*sb + sh*cb,
      ca*cb,              -sa,      -ca*sb,
      sh*sa*cb + ch*sb,    sh*ca,   -sh*sa*sb + ch*cb
    ]; // Includes 90-degree rotation around z axis

  /* [m00 m01 m02] 0 1 2
   * [m10 m11 m12] 3 4 5
   * [m20 m21 m22] 6 7 8 */

  if (matrix[3] > 0.9999)
  {
    // Deal with singularity at north pole
    heading = Math.atan2(matrix[2],matrix[8]);
    attitude = Math.PI/2;
    bank = 0;
  }
  else if (matrix[3] < -0.9999)
  {
    // Deal with singularity at south pole
    heading = Math.atan2(matrix[2],matrix[8]);
    attitude = -Math.PI/2;
    bank = 0;
  }
  else
  {
    heading = Math.atan2(-matrix[6],matrix[0]);
    bank = Math.atan2(-matrix[5],matrix[4]);
    attitude = Math.asin(matrix[3]);
  }

  result.yaw = heading;
  result.pitch = attitude;
  result.roll = bank;
}
'use strict';

function VideoAsset(videoElement) {
    this._videoElement = null;
    this._destroyed = false;
    this._emitChange = this.emit.bind(this, 'change');
    this._lastTimestamp = -1;

    this._emptyCanvas = document.createElement('canvas');
    this._emptyCanvas.width = 1;
    this._emptyCanvas.height = 1;

    this.setVideo(videoElement);
}

Marzipano.dependencies.eventEmitter(VideoAsset);

VideoAsset.prototype.dynamic = true;

VideoAsset.prototype.setVideo = function(videoElement) {
    var self = this;

    if (this._videoElement) {
        this._videoElement.removeEventListener('timeupdate', this._emitChange);
    }

    this._videoElement = videoElement;

    if (!this._videoElement) {
        return;
    }

    this._videoElement.addEventListener('timeupdate', this._emitChange);

    // Emit a change event on every frame while the video is playing.
    // TODO: make the loop sleep when video is not playing.
    if (this._emitChangeIfPlayingLoop) {
        cancelAnimationFrame(this._emitChangeIfPlayingLoop);
        this._emitChangeIfPlayingLoop = null;
    }

    function emitChangeIfPlaying() {
        if (!self._videoElement.paused) {
            self.emit('change');
        }
        if (!self._destroyed) {
            self._emitChangeIfPlayingLoop = requestAnimationFrame(emitChangeIfPlaying);
        }
    }
    emitChangeIfPlaying();

    this.emit('change');
};

VideoAsset.prototype.width = function() {
    if (this._videoElement) {
        return this._videoElement.videoWidth;
    } else {
        return this._emptyCanvas.width;
    }
};

VideoAsset.prototype.height = function() {
    if (this._videoElement) {
        return this._videoElement.videoHeight;
    } else {
        return this._emptyCanvas.height;
    }
};

VideoAsset.prototype.element = function() {
    if (this._videoElement) {
        return this._videoElement;
    } else {
        return this._emptyCanvas;
    }
};

VideoAsset.prototype.timestamp = function() {
    if (this._videoElement) {
        this._lastTimestamp = this._videoElement.currentTime;
    }
    return this._lastTimestamp;
};

VideoAsset.prototype.destroy = function() {
    this._destroyed = true;
    if (this._videoElement) {
        this._videoElement.removeEventListener('timeupdate', this._emitChange);
    }
    if (this._emitChangeIfPlayingLoop) {
        cancelAnimationFrame(this._emitChangeIfPlayingLoop);
        this._emitChangeIfPlayingLoop = null;
    }
};// CALLBACKS HELPER
function CallbackHelper() {
    this.callbacks = {};
}

CallbackHelper.prototype.append = function(key, callback) {
    if(this.callbacks[key] === undefined)
        this.callbacks[key] = [];

    this.callbacks[key].push(callback);
};

CallbackHelper.prototype.dispatch = function(key, args) {
    if(this.callbacks[key] === undefined)
        return true;

    $.each(this.callbacks[key], function(index, cb) {
        if(typeof cb === 'function')
            cb(args);
    });
};function Controls(config) {
    this.defaultConfig = {
        viewer: null,
        callbacks: new CallbackHelper(),
        elements: {
            up: $('#viewUp'),
            down: $('#viewDown'),
            left: $('#viewLeft'),
            right: $('#viewRight'),
            viewIn: $('#viewIn'),
            viewOut: $('#viewOut'),
            play: $("a#play"),
            pause: $("a#pause"),
            autorotate: $('#autorotateToggle'),
            fullscreen: $('#fullscreenToggle'),
            orientation: $('#toggleDeviceOrientation')
        },
        velocity: 0.7,
        friction: 3,
        mobile: true,
        isMobile: false,
        defaultMobileState: true
    };
    console.log('orientation');
    this.config = $.extend(this.defaultConfig, config);
    this.viewUpElement = this.config.elements.up;
    this.viewDownElement = this.config.elements.down;
    this.viewLeftElement = this.config.elements.left;
    this.viewRightElement = this.config.elements.right;
    this.viewInElement = this.config.elements.viewIn;
    this.viewOutElement = this.config.elements.viewOut;
    this.pauseElement = this.config.elements.pause;
    this.playElement = this.config.elements.play;
    this.toggleDeviceOrientation = this.config.elements.orientation;
    this.defaultMobileState = this.config.defaultMobileState;

    this.viewer = this.config.viewer;
    this.callbacks = this.config.callbacks;
    this.velocity = this.config.velocity;
    this.friction = this.config.friction;
    this.deviceOrientation = null;
    this.orientationEnabled = false;

    this.controls = this.viewer.controls();
}

Controls.prototype.enableMobile = function() {
    this.deviceOrientation = new DeviceOrientationControlMethod();

    this.controls.registerMethod('deviceOrientation', this.deviceOrientation);

    var self = this;
    this.toggleDeviceOrientation.click(function() {
        if(self.orientationEnabled === false ){
            self.activateDeviceOrientation();
            console.log("test");
        } else {
            self.deactivateDeviceOrientation();
        }
    });

    if(this.defaultMobileState === true) {
        this.activateDeviceOrientation();
    } else {
        this.deactivateDeviceOrientation();
    }
};

Controls.prototype.deactivateDeviceOrientation = function() {
    this.orientationEnabled = false;
    this.controls.disableMethod('deviceOrientation');
};

Controls.prototype.activateDeviceOrientation = function() {
    var self = this;
    if(this.orientationEnabled === false ){
        this.deviceOrientation.getPitch(function(err, pitch) {
            //self.callbacks.dispatch("changePitch", {pitch: pitch, err: err});
        });
        this.controls.enableMethod('deviceOrientation');
        this.orientationEnabled = true;
    }
};

Controls.prototype.on = function(event, cb) {
    this.callbacks.append(event, cb);
};

Controls.prototype.init = function(marzipano) {
    var self = this;
    if(this.viewLeftElement.length > 0) {
        this.controls.registerMethod('leftElement', new marzipano.ElementPressControlMethod(this.viewLeftElement.get(0), 'x', -this.velocity, this.friction), true);
    }

    if(this.viewRightElement.length > 0) {
        this.controls.registerMethod('rightElement', new marzipano.ElementPressControlMethod(this.viewRightElement.get(0),  'x',  this.velocity, this.friction), true);
    }

    if(this.viewInElement.length > 0) {
        this.controls.registerMethod('inElement',    new marzipano.ElementPressControlMethod(this.viewInElement.get(0),  'zoom', -this.velocity, this.friction), true);
    }

    if(this.viewOutElement.length > 0) {
        this.controls.registerMethod('outElement',   new marzipano.ElementPressControlMethod(this.viewOutElement.get(0), 'zoom',  this.velocity, this.friction), true);
    }

    if(this.viewUpElement.length > 0) {
        this.controls.registerMethod('upElement',    new marzipano.ElementPressControlMethod(this.viewUpElement.get(0),     'y', -this.velocity, this.friction), true);
    }

    if(this.viewDownElement.length > 0) {
        this.controls.registerMethod('downElement', new marzipano.ElementPressControlMethod(this.viewDownElement.get(0), 'y', this.velocity, this.friction), true);
    }

    if(this.playElement.length > 0) {
        this.playElement.click(function() {
            self.callbacks.dispatch("play");
            self.playElement.css("display", "none");
            if(self.pauseElement.length > 0) {
                self.pauseElement.css('display', 'block');
            }
        });
    }

    if(this.pauseElement.length > 0) {
        this.pauseElement.click(function() {
            self.callbacks.dispatch("pause");
            self.pauseElement.css("display", "none");
            if(self.playElement.length > 0) {
                self.playElement.css('display', 'block');
            }
        });
    }

    if(this.config.mobile && this.config.isMobile) {
        this.enableMobile();
    }
};/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 *
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);function Scene(config){
    this.defaultConfig = {
        dir: "",
        data: {},
        marzipano: null,
        view: null,
        viewer: null,
        isMobile: true,
    };

    var self = this;
    this.config = $.extend(this.defaultConfig, config);
    this.data = this.config.data;

    this.playOnce = false;
    if(this.data.playOnce !== undefined) {
        this.playOnce = (this.data.playOnce === "1") ? true : ((this.data.playOnce === "true") ? true : ((this.data.playOnce === true) ? true : false));
    }

    this.played = false;
    if(this.config.isMobile) {
        console.log("mobile");
        this.played = true;
        this.playOnce = true;
    }
    this.replaceWith = null;


    this.urlPrefix = "tiles/"+this.config.dir;

    if(this.data.fileType === undefined) {
        this.data.fileType = this.guessType();
    }

    if("image" === this.data.fileType) {
        this.fromImage();
    } else if("video" === this.data.fileType) {
        this.fromVideo();
    }

    this.scene = this.config.viewer.createScene({
        source: this.source,
        geometry: this.geometry,
        view: this.view,
        pinFirstLevel: true
    });

    this.config.view.on('loaded', function() {
        // Create link hotspots.
        self.data.linkHotspots.forEach(function(hotspot) {
            var element = self.createHotSpot(hotspot);
            self.scene.hotspotContainer().createHotspot(element.get(0), { yaw: hotspot.yaw, pitch: hotspot.pitch });
        });

        // Create info hotspots.
        self.data.infoHotspots.forEach(function(hotspot) {
            var element = self.createInfoHotspotElement(hotspot);
            self.scene.hotspotContainer().createHotspot(element.get(0), { yaw: hotspot.yaw, pitch: hotspot.pitch });
        });


        if(self.data.replaceWith !== undefined) {
            self.replaceWith = self.config.view.findSceneById(self.data.replaceWith);
        }
    });

    this.config.view.scene().on('change', function(scene) {
        if(scene.data.id === self.data.id) {
            self.config.view.stopAutorotate();
            if(self.data.fileType === "video") {
                self.startVideo();
            } else {
                self.config.view.startAutorotate();
            }
        } else {
            if(self.data.fileType === "video") {
                self.stopVideo();
            }
        }
    });
}

Scene.prototype.waitForReadyState = function (element, readyState, interval, done) {
    var timer = setInterval(function() {
        if (element.readyState >= readyState) {
            clearInterval(timer);
            done(null, true);
        }
    }, interval);
};

Scene.prototype.startVideo = function() {
    this.video.autoplay = true;
    this.video.play();

    if(this.played && this.playOnce === true) {
        this.video.currentTime = this.video.duration;
    }

    this.view.setYaw(this.data.initialViewParameters.yaw);
    this.view.setPitch(this.data.initialViewParameters.yaw);
    this.view.setFov(this.data.initialViewParameters.fov);
    if (this.video.readyState >= this.video.HAVE_ENOUGH_DATA) {
        //this.config.view.startAutorotate();
    }
    this.played = true;
};

Scene.prototype.stopVideo = function() {
    this.video.pause();
};

Scene.prototype.fromVideo = function() {
    var asset = new VideoAsset();
    var self = this;
    this.source = new this.config.marzipano.SingleAssetSource(asset);
    this.geometry = new this.config.marzipano.EquirectGeometry([ { width: 1 } ]);

    this.limiter = this.config.marzipano.RectilinearView.limit.vfov(90*Math.PI/180, 90*Math.PI/180);
    this.view = new this.config.marzipano.RectilinearView(this.data.initialViewParameters, this.limiter);

    this.video = document.createElement('video');
    this.video.src = this.urlPrefix + "/" + this.data.id + "/video.mp4";
    this.video.crossOrigin = 'anonymous';

    this.video.playsInline = true;
    this.video.webkitPlaysInline = true;

    self.waitForReadyState(self.video, self.video.HAVE_ENOUGH_DATA, 100, function() {
        self.config.view.stopAutorotate();
        asset.setVideo(self.video);
        self.config.view.startAutorotate();
    });
};

Scene.prototype.guessType = function() {
    return 'image';
};

Scene.prototype.fromImage = function() {
    this.source = this.config.marzipano.ImageUrlSource.fromString(
        this.urlPrefix + "/" + this.data.id + "/{z}/{f}/{y}/{x}.jpg",
        { cubeMapPreviewUrl: this.urlPrefix + "/" + this.data.id + "/preview.jpg" });
    this.geometry = new this.config.marzipano.CubeGeometry(this.data.levels);

    this.limiter = this.config.marzipano.RectilinearView.limit.traditional(this.data.faceSize, 100*Math.PI/180, 120*Math.PI/180);
    this.view = new this.config.marzipano.RectilinearView(this.data.initialViewParameters, this.limiter);


};

Scene.prototype.getData = function() {
    if(this.played && this.playOnce && this.replaceWith !== null) {
        return this.replaceWith.getData();
    }

    return this.data;
};

Scene.prototype.getScene = function() {
    if(this.played && this.playOnce && this.replaceWith !== null) {
        return this.replaceWith.getScene();
    }
    return this.scene;
};

Scene.prototype.getView = function() {
    if(this.played && this.playOnce && this.replaceWith !== null) {
        return this.replaceWith.getView();
    }
    return this.view;
};

Scene.prototype.get = function() {
    return {
        data: this.getData(),
        scene: this.getScene(),
        view: this.getView()
    }
};

Scene.prototype.createInfoHotspotElement = function(hotspot) {

    // Create wrapper element to hold icon and tooltip.
    var wrapper = $(document.createElement('div'));
    wrapper.addClass('hotspot');
    wrapper.addClass('info-hotspot');

    // Create hotspot/tooltip header.
    var header = $(document.createElement('div'));
    header.addClass('info-hotspot-header');

    // Create image element.
    var iconWrapper = $(document.createElement('div'));
    iconWrapper.addClass('info-hotspot-icon-wrapper');
    var icon = $(document.createElement('img'));
    icon.attr('src','assets/img/info.png');
    icon.addClass('info-hotspot-icon');
    iconWrapper.append(icon);

    // Create title element.
    var titleWrapper = $(document.createElement('div'));
    titleWrapper.addClass('info-hotspot-title-wrapper');
    var title = $(document.createElement('div'));
    title.addClass('info-hotspot-title');
    title.html(hotspot.title);
    titleWrapper.append(title);

    // Create close element.
    var closeWrapper = $(document.createElement('div'));
    closeWrapper.addClass('info-hotspot-close-wrapper');
    var closeIcon = $(document.createElement('img'));
    closeIcon.attr('src', 'assets/img/close.png');
    closeIcon.addClass('info-hotspot-close-icon');
    closeWrapper.append(closeIcon);

    // Construct header element.
    header.append(iconWrapper);
    header.append(titleWrapper);
    header.append(closeWrapper);

    // Create text element.
    var text = $(document.createElement('div'));
    text.addClass('info-hotspot-text');
    text.html(hotspot.text);

    // Place header and text into wrapper element.
    wrapper.append(header);
    wrapper.append(text);

    // Create a modal for the hotspot content to appear on mobile mode.
    var modal = $(document.createElement('div'));
    modal.html(wrapper.html());
    modal.addClass('info-hotspot-modal');
    $('body').append(modal);

    var toggle = function() {
        wrapper.toggleClass('visible');
        modal.toggleClass('visible');
    };

    // Show content when hotspot is clicked.
    wrapper.find('.info-hotspot-header').on('click', toggle);

    // Hide content when close icon is clicked.
    modal.find('.info-hotspot-close-wrapper').on('click', toggle);

    // Prevent touch and scroll events from reaching the parent element.
    // This prevents the view control logic from interfering with the hotspot.
    this.stopEventPropagation(wrapper);

    return wrapper;
};

Scene.prototype.stopEventPropagation = function(wrapper) {
    var eventList = [ 'touchstart', 'touchmove', 'touchend', 'touchcancel',
        'wheel', 'mousewheel' ];
    for (var i = 0; i < eventList.length; i++) {
        wrapper.on(eventList[i], function(event) {
            event.stopPropagation();
        });
    }
};

Scene.prototype.createHotSpot = function(hotspot) {
    var self = this;
    // Create wrapper element to hold icon and tooltip.
    var wrapper = $(document.createElement('div'));
    wrapper.addClass('hotspot');
    wrapper.addClass('link-hotspot');

    // Create image element.
    var icon = $(document.createElement('img'));
    icon.attr('src', 'assets/img/link.png');
    icon.addClass('link-hotspot-icon');

    $(function(){
    $(icon).mouseover(function(){
        icon.attr('src', 'assets/img/link-clic.png');
    });
    $(icon).mouseout(function(){
            icon.attr('src', 'assets/img/link.png');
    });
});

    // Set rotation transform.
    var transformProperties = [ '-ms-transform', '-webkit-transform', 'transform' ];
    for (var i = 0; i < transformProperties.length; i++) {
        var property = transformProperties[i];
        icon.css(property, 'rotate(' + hotspot.rotation + 'rad)');
    }

    // Add click event handler.
    wrapper.on('click', function() {
        self.config.view.switchScene(self.config.view.findSceneById(hotspot.target));
    });

    // Prevent touch and scroll events from reaching the parent element.
    // This prevents the view control logic from interfering with the hotspot.
    this.stopEventPropagation(wrapper);

    // Create tooltip element.
    var tooltip = $(document.createElement('div'));
    tooltip.addClass('hotspot-tooltip');
    tooltip.addClass('link-hotspot-tooltip');
    tooltip.html(this.config.view.findSceneDataById(hotspot.target).name);

    wrapper.append(icon);
    wrapper.append(tooltip);

    return wrapper;
};function View(config) {
    var self = this;
    this.defaultConfig = {
        container: $('#pano'),
        elements: {
            up: $('#viewUp'),
            down: $('#viewDown'),
            left: $('#viewLeft'),
            right: $('#viewRight'),
            viewIn: $('#viewIn'),
            viewOut: $('#viewOut'),
            play: $("a#play"),
            pause: $("a#pause"),
            autorotate: $('#autorotateToggle'),
            fullscreen: $('#fullscreenToggle'),
            orientation: $('#toggleDeviceOrientation')
        },
        autorotate: true,
        allowFullscreen: true,
        mobile: true,
        detectMobile: function() {
            return jQuery.browser.mobile;
        },
        defaultMobileState: true,
        viewerOpts: {
            controls: {
                mouseViewMode: "drag"
            },
            stageType: 'webgl',
        },
        events: {
            onChangeScene: null,
            onLoaded: null,
        }
    };

    this.config = $.extend(this.defaultConfig, config);
    this.container = this.config.container;
    this.marzipano = window.Marzipano;
    this.bowser = window.bowser;
    this.screenfull = window.screenfull;
    this.autorotate = null;
    this.currentScene = null;

    this.autorotateActive = false;

    this.autorotateToggleElement = this.config.elements.autorotate;
    this.fullscreenToggleElement = this.config.elements.fullscreen;

    this.callbacks = {
        scene: new CallbackHelper(),
        view: new CallbackHelper(),
        generic: new CallbackHelper(),
    };


    this._deletable = [];

    if(this.config.events.onChangeScene !== null) {
        this.callbacks.scene.append("change", this.config.events.onChangeScene);
    }

    if(this.config.events.onLoaded !== null) {
        this.callbacks.generic.append("loaded", this.config.events.onLoaded);
    }



    this.scenes = {};
    this.viewer = new this.marzipano.Viewer(this.config.container.get(0),this.config.viewerOpts);
    this.controls = new Controls({
        elements: this.config.elements,
        viewer: this.viewer,
        mobile: this.config.mobile,
        isMobile: this.config.detectMobile(),
        defaultMobileState: this.config.defaultMobileState,
    });

    this.detectScreen();
    this.detectTouchDevice();
    if(this.config.autorotate) {
        this.enableAutorotate();
        this.startAutorotate();
    }
    if(this.config.allowFullscreen) {
        this.enableFullscreen();
    }

    this.initEvents();
}

View.prototype.scene = function() {
    var self = this;

    return {
        on: function(event, cb) {
            self.callbacks.scene.append(event, cb);
        }
    };
};

View.prototype.load = function(url, cb) {
    var self = this;
    $.ajax({
        url: url
    }).done(function(data) {
        self.createScenes(data.tiles_dir, data.scenes);
        self.callbacks.generic.dispatch("loaded", data);
    }).fail(function(xhr, status, error) {
        console.log(status, error);
    });
};

View.prototype.on = function(event, cb) {
    this.callbacks.generic.append(event, cb);
};

View.prototype.createScenes = function(dir, scenes) {
    this.data = scenes;
    var self = this;
    var firstScene = null;
    $.each(scenes, function(index, data) {
        self.scenes[data.id] = new Scene({
            dir: dir,
            marzipano: self.marzipano,
            data: data,
            view: self,
            viewer: self.viewer,
            isMobile: self.config.detectMobile()
        });
        if(data.first === true) {
            firstScene = self.scenes[data.id];
        }
    });



    this.switchScene(firstScene);
};

View.prototype.findSceneById = function(id) {
    if(this.scenes[id] === undefined)
        return null;

    return this.scenes[id];
};

View.prototype.findSceneDataById = function(id) {
    var scene = this.findSceneById(id);
    if(scene === null)
        return null;

    return scene.getData();
};

View.prototype.switchScene = function(scene) {
    scene.getView().setParameters(scene.getData().initialViewParameters);
    scene.getScene().switchTo();
    this.currentScene = scene;
};

View.prototype.updateSceneList = function(scene) {
    /*
    for (var i = 0; i < sceneElements.length; i++) {
        var el = sceneElements[i];
        if (el.getAttribute('data-id') === scene.data.id) {
            el.addClass('current');
        } else {
            el.classList.remove('current');
        }
    }*/
};

View.prototype.updateSceneName = function(scene) {
    //sceneNameElement.innerHTML = sanitize(scene.data.name);
};

View.prototype.sanitize = function(text) {
    return text.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;');
};

View.prototype.detectScreen = function() {
    if (window.matchMedia) {
        var setMode = function() {
            if (mql.matches) {
                $("body").removeClass("desktop");
                $("body").addClass("mobile");
            } else {
                $("body").addClass("desktop");
                $("body").removeClass("mobile");
            }
        };
        var mql = matchMedia("(max-width: 500px), (max-height: 500px)");
        setMode();
        mql.addListener(setMode);
    } else {
        $("body").addClass("desktop");
    }
};

View.prototype.startAutorotate = function() {

    if(this.autorotateActive === false ) {
        this.autorotateActive = true;
        this.viewer.startMovement(this.autorotate);
        this.viewer.setIdleMovement(3000, this.autorotate);
        this.callbacks.generic.dispatch("startRotate");
    }
}

View.prototype.stopAutorotate = function() {
    if(this.autorotateActive === true ) {
        this.autorotateActive = false;
        this.viewer.stopMovement();
        this.viewer.setIdleMovement(Infinity);
        this.callbacks.generic.dispatch("stopRotate");
    }
}

View.prototype.enableAutorotate = function() {
    this.autorotate = this.marzipano.autorotate({
        yawSpeed: 0.03,
        targetPitch: 0,
        targetFov: Math.PI/2
    });

    var self = this;
    this.autorotateToggleElement.on('click', function() {
        if (self.autorotateActive) {
            self.stopAutorotate();
        } else {
            self.startAutorotate();
        }
    });
};

View.prototype.initEvents = function() {
    if (!$("body").hasClass('mobile')) {
        this.showSceneList();
    }

    var self = this;

    this.viewer.addEventListener("sceneChange", function() {
        $.each(self.scenes, function(index, s) {
            if(s.getScene() === self.viewer._scene) {
                self.callbacks.scene.dispatch("change", s);
            }
        });
    });


    this.controls.init(this.marzipano);
    this.controls.on("play", function() {
        self.startAutorotate();
    });

    this.controls.on("pause", function() {
        self.stopAutorotate();
    });

    this.controls.on('changePitch', function(args) {
        if (!args.err) {
            if(self.currentScene === null) {
                self.on('loaded', function() {
                    self.currentScene.getView().setPitch(args.pitch);
                });
            } else {
                self.currentScene.getView().setPitch(args.pitch);
            }
        }
    });
};

View.prototype.showSceneList = function() {
    //sceneListElement.classList.add('enabled');
    //sceneListToggleElement.classList.add('enabled');
};

View.prototype.enableFullscreen = function() {
    if (this.screenfull.enabled) {
        $("body").addClass('fullscreen-enabled');
        var self = this;
        this.fullscreenToggleElement.on('click', function() {
            self.toggleFullscreen();
        });
    } else {
        $("body").addClass('fullscreen-disabled');
    }

};

View.prototype.toggleSceneList = function(){
    /*
    sceneListElement.classList.toggle('enabled');
    sceneListToggleElement.classList.toggle('enabled');
    */
};

View.prototype.hideSceneList = function() {

    //sceneListElement.classList.remove('enabled');
   //sceneListToggleElement.classList.remove('enabled');
};

View.prototype.toggleFullscreen = function() {
    this.screenfull.toggle();
    if (this.screenfull.isFullscreen) {
        this.fullscreenToggleElement.addClass('enabled');
        this.callbacks.generic.dispatch("fullscreen", {enabled: false});
    } else {
        this.fullscreenToggleElement.removeClass('enabled');
        this.callbacks.generic.dispatch("fullscreen", {enabled: true});
    }
};

View.prototype.detectTouchDevice = function() {
    // Detect whether we are on a touch device.
    $("body").addClass("no-touch");

    window.addEventListener('touchstart', function() {
        $("body").removeClass("no-touch");
        $("body").addClass("touch");
    });
};

View.prototype.ieFallback = function() {
    if (this.bowser.msie && parseFloat(this.bowser.version) < 11) {
        $("body").addClass('tooltip-fallback');
    }
};


//# sourceMappingURL=main.js.map
