var _latitude = 49.0525847;
var _longitude = -1.0368834;
var jsonPath = 'assets/json/items.json.txt';

// Load JSON data and create Google Maps
$.getJSON(jsonPath)
   .done(function(json) {
        createHomepageGoogleMap(_latitude,_longitude,json);
    })
    .fail(function( jqxhr, textStatus, error ) {
    })
;

// Set if language is RTL and load Owl Carousel
$(window).load(function(){
    var rtl = false; // Use RTL
    initializeOwl(rtl);
});

autoComplete();
